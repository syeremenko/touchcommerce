(function() {
 $('#survey').validate({
   rules: {
     name: 'required',
     phone: {
       required: true,
       phoneUS: true
     },
     onkeyup: false,
     onclick: false,
     focusInvalid: false
   },
   messages: {
     name: {
       required: 'Full Name is a required field.'
     },
     phone: {
       required: 'Mobile number is a required field.',
       phoneUS: 'Please enter a valid phone number.'
     }
   },
 });
}());
